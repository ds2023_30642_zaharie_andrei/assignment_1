import React, {useContext, useState} from 'react';
import logo from './logo.png';
import './App.css';
import {Navigate, Route, Routes, useNavigate} from "react-router-dom";
import Login from './pages/authentication/Login'
import Home from "./pages/Home";
import Register from './pages/authentication/Register';
import Header from "./pages/header/Header";
import UserProvider, {UserContext} from "./pages/UserProvider";
import PrivateRoute from "./pages/PrivateRoute";


function App() {
    const [value, setValue] = useState(false);
    const {token, setToken} = useContext(UserContext);
    let navigate = useNavigate();
  return (
      <div className="App">
    <UserProvider>
          <Header />
        <Routes>
            <Route path="/" element={<PrivateRoute />}>
                <Route index element={<Home />} />
            </Route>
            <Route path="/register" element={<Register />} />
            <Route path="/login" element ={<Login />} />

        </Routes>
    </UserProvider>
      </div>

  );
}

export default App;
