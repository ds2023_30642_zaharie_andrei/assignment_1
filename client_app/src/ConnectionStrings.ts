export type ConnectionStrings = {
    userLocalHost: 'http://localhost:8080/api/';
    deviceLocalHost: 'http://localhost:8081/api/';
}