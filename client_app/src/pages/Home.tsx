import {useContext, useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {CircularProgress} from "@mui/material";
import Header from "./header/Header"
import axios from "axios";
import AdminPage from "./users/Admin/AdminPage";
import {UserContext} from "./UserProvider";
import UserPage from "./users/Client/UserPage";


export default function Home() {
    const {token, setToken} = useContext(UserContext);
    let navigate = useNavigate();
    const email = localStorage.getItem("email");
    const role = localStorage.getItem("role");
    const [isAdmin, setIsAdmin] = useState(false);
    const userApi= axios.create({
        baseURL: "http://localhost:8080/api/"
    });

    useEffect(() => {
        if(role === 'user'){
            setIsAdmin(false);
        }else if(role === 'admin'){
            setIsAdmin(true);
        }
    }, [token, role]);


    async function makeRequest() {
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
       // setLoading(true);
        const result = await fetch('http://localhost:8080/api/users/hello', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        let data = await result.json();
        console.log(data);
       // setLoading(false);
        if(result.ok){

        }
        else{
           // setError('Invalid login!');
        }
    }

    return (
        <div>
            {isAdmin?(
                <AdminPage />
            ):(
                <UserPage />
            )
            }

        </div>
    );
}
