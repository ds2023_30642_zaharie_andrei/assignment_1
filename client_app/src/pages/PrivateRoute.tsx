import React, {useContext} from "react";
import {Navigate, Outlet} from "react-router-dom";
import {UserContext} from "./UserProvider";

const PrivateRoute: React.FC = () => {
    const { token } = useContext(UserContext);

    return token ? <Outlet /> : <Navigate to="/login" />;
};


export default PrivateRoute;