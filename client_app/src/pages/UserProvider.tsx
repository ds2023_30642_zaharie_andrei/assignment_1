import axios from "axios";
import { createContext, useContext, useEffect, useMemo, useState } from "react";

interface UserContetType {
    token: string | null;
    setToken: (newToken: string | null) => void;
    isLogged: boolean | null;
    setIsLogged: (logged : boolean | null) => void;
    refreshPage: boolean | null;
    setRefreshPage: (refresh : boolean | null) => void;
    userGuid: string | null;
    setUserGuid: (guid : string | null) => void;
}

export const UserContext = createContext<UserContetType>({
    token: null,
    setToken: () => {},
    isLogged: false,
    setIsLogged: () => {},
    refreshPage: true,
    setRefreshPage: () =>{},
    userGuid: null,
    setUserGuid: () => {},
});

const UserProvider = ({ children }: { children: React.ReactNode }) => {
    const [token, setToken_] = useState<string | null>(
        localStorage.getItem("JwtToken")
    );
    const [isLogged, setIsLogged_] = useState<boolean | null>(false);
    const [refreshPage, setRefreshPage_] = useState<boolean | null>(true);
    const [userGuid, setUserGuid_] = useState<string | null>(localStorage.getItem("guid"));

    const setToken = (newToken: string | null) => {
        setToken_(newToken);
    };

    const setIsLogged = (logged: boolean | null) => {
        setIsLogged_(logged);
    };

    const setUserGuid = (guid: string | null) => {
        setUserGuid_(guid);
    };

    const setRefreshPage = (refresh: boolean | null) => {
        setRefreshPage_(refresh);
    };

    useEffect(() => {
        if (token) {
           // axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
            let jwtPayload = JSON.parse(atob(token.split('.')[1]));
            const email = jwtPayload.email;
            const role = jwtPayload.role;
            const guid = jwtPayload.guid
            localStorage.setItem("email", email);
            localStorage.setItem("role", role);
            localStorage.setItem("guid", guid);
            setIsLogged(true);
        } else {
            localStorage.clear();
        }
    }, [token]);

    const contextValue = useMemo(
        () => ({
            token,
            setToken,
            isLogged,
            setIsLogged,
            refreshPage,
            setRefreshPage,
            userGuid,
            setUserGuid,
        }),
        [userGuid,token, isLogged, refreshPage]
    );

    return (
        <UserContext.Provider value={contextValue}>
            {children}
        </UserContext.Provider>
    );
};

export default UserProvider;