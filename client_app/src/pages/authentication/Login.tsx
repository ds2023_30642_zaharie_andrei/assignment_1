import React, {Component, useContext, useState} from 'react';
import {useNavigate} from "react-router-dom";
import {IUserLogin} from "./Authentication.types";
import "./Authentication.styles.css"
import "../../index.css"
import {CircularProgress} from "@mui/material";
import {ConnectionStrings} from "../../ConnectionStrings";
import {UserContext} from "../UserProvider";


export default function Login(){
    const [error, setError] = useState<string>();
    const [loading, setLoading] = useState(false);
    //const [token, setToken] = useState(null);
    const {token, setToken} = useContext(UserContext);
    const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    let navigate = useNavigate();
    const [userLogin, setUser] = useState<IUserLogin>({
        email: "",
        password: "",
    });

    async function loginUser(credentials : any) {
        setLoading(true);
        const result = await fetch('http://localhost:8080/api/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        });
        let data = await result.json();
        setLoading(false);
        if(result.ok){
            localStorage.setItem("JwtToken", data.token);
            setToken(data.token);
            navigate('/');
        }
        else{
            setError('Invalid login!');
        }
    }

    async function goToRegister(e : any){
        navigate("/register");
    }

    const onInputChange = (e:any) => {
        setError("");
        setUser({ ...userLogin, [e.target.name]: e.target.value });
    };


    const handleSubmit = async (e:any) => {
        e.preventDefault();
        if(!emailRegex.test(userLogin.email)){
            setError("Invalid email structure!");
            return;
        }
        await loginUser(userLogin);
    }


    return (
        <div>
        {loading ?(
                <div className="loader-container">
                    <CircularProgress />
                </div>
            ):(
        <div className="login-container">
            <div className="login-box">
                <h2>Welcome!</h2>
                <p className="subtitle">Login into your account</p>
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="username">Email</label>
                        <input
                            placeholder="Email"
                            type="text"
                            id="email"
                            name="email"
                            value={userLogin.email}
                            onChange={onInputChange}
                            required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                            placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;"
                            type="password"
                            id="password"
                            name="password"
                            value={userLogin.password}
                            onChange={onInputChange}
                            required
                        />
                    </div>
                    {error && <p className="styledError">{error}</p>}
                    <button type="submit">Login</button>
                </form>
                <p>Don't have an account?
                    <a href={""} onClick={goToRegister}> Register</a>
                </p>
            </div>
        </div>)}
        </div>
    );
}
