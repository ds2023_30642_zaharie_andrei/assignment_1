import React, {useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";
import "./Authentication.styles.css";
import {CircularProgress} from "@mui/material";
import {IUserRegisterRequest} from "./Authentication.types";
interface RegisterForm {
    name: string;
    email: string;
    password: string;
    repeatPassword: string;
}

export default function Register() {
    const [formData, setFormData] = useState<RegisterForm>({name:'', email: '', password: '' , repeatPassword: ''});
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<string>();
    const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    let navigate = useNavigate();
    const onInputChange = (e: any) => {
        setError("");
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    useEffect(() => {

    }, []);

    async function registerUser(registerData: any) {

        if(registerData.password !== registerData.repeatPassword){
            setError("Passwords don't match!")
            return;
        }
        let loginRequest : IUserRegisterRequest = {
            email: registerData.email,
            name: registerData.name,
            password: registerData.password,
            role: "user"
        }
        console.log(loginRequest);
        setLoading(true);
        const result = await fetch('http://localhost:8080/api/auth/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loginRequest)
        });
        let data = await result.json();
        setLoading(false);
        if (result.ok) {
            console.log("OK!");
            console.log(data);
            localStorage.setItem("JwtToken", data.token);
            navigate('/');
        } else {
            setError('Invalid Register!');
        }
    }
    const handleSubmit = async (e:any) => {
        e.preventDefault();
        if(!emailRegex.test(formData.email)){
            setError("Invalid email structure!")
            return;
        }
        await registerUser(formData);
    }
    async function navigateToLogin(){
        navigate("/login");
    }

    return (
        <div>
        { loading?(
            <div className="loader-container">
                <CircularProgress />
            </div>
        ):(
        <div className="register-container">
            <div className="register-box">
                <h2>Hello there!</h2>
                <p className="subtitle">Create and account</p>
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input
                            placeholder="Email"
                            type="text"
                            id="email"
                            name="email"
                            value={formData.email}
                            onChange={onInputChange}
                            required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Full Name</label>
                        <input
                            placeholder="Jhon Doe"
                            type="text"
                            id="name"
                            name="name"
                            value={formData.name}
                            onChange={onInputChange}
                            required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="passsword">Password</label>
                        <input
                            placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;"
                            type="password"
                            id="password"
                            name="password"
                            value={formData.password}
                            onChange={onInputChange}
                            required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="repeatPassword">Repeat Password</label>
                        <input
                            placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;"
                            type="password"
                            id="repeatPassword"
                            name="repeatPassword"
                            value={formData.repeatPassword}
                            onChange={onInputChange}
                            required
                        />
                    </div>
                    {error && <p className="styledError">{error}</p>}
                    <button type="submit">Register</button>
                </form>
                <p>Already have an account?
                    <a href={""} onClick={navigateToLogin}>Login</a>
                </p>
            </div>
        </div>
            )}
        </div>
    );
};