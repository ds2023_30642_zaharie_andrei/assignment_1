import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import {useNavigate} from "react-router-dom";
import "./Header.styles.css"
import {Devices} from "@mui/icons-material";
import {useEffect, useState, useContext} from "react";
import {UserContext} from "../UserProvider";

const pages = ['Devices', 'Dashboard', 'Login'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];
export default function Header() {
    let navigate = useNavigate();
    const [anchorElNav, setAnchorElNav] = useState<null | HTMLElement>(null);
    const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);
    //const[token, setToken]= useState(localStorage.getItem("JwtToken"));
    const{token,setToken, isLogged, setIsLogged} = useContext(UserContext);

    useEffect(() => {

    }, [isLogged]);
    const logoutButton = async (e:any) => {
        setToken(null);
        setIsLogged(false);
        navigate('/login');
    }
    const buttonClicked = (event: React.MouseEvent<HTMLElement>) =>{
        console.log(event.currentTarget.textContent);
        navigate("/" + event.currentTarget.textContent);
    }

    const settingsClicked = (event: React.MouseEvent<HTMLElement>) =>{
        console.log(event.currentTarget.textContent);
        if(event.currentTarget.textContent === 'Logout'){
            setIsLogged(false);
            setToken(null);
            navigate("/login");
        }else {
            navigate("/" + event.currentTarget.textContent);
        }
    }

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    return (
        <div className={"header"}>

            <AppBar position="static">
                <Container maxWidth="xl">
                    <Toolbar >
                        <div className={"catalogName"}>
                            <h4>
                                Device Manager
                            </h4>
                        </div>

                        <AdbIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />
                        <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                            <Button
                                value={"Devices"}
                                onClick={buttonClicked}
                                sx={{ my: 2, color: 'white', display: 'block' }}
                            >
                                Devices
                            </Button>
                            <Button
                                value={"Users"}
                                onClick={buttonClicked}
                                sx={{ my: 2, color: 'white', display: 'block' }}
                            >
                                Users
                            </Button>
                            {
                                isLogged ? (
                                    <Button
                                        value={"Logout"}
                                        onClick={logoutButton}
                                        sx={{ my: 2, color: 'white', display: 'block' }}
                                    >
                                        Log out
                                    </Button>
                                ):(
                                    <Button
                                        value={"Login"}
                                        onClick={logoutButton}
                                        sx={{ my: 2, color: 'white', display: 'block' }}
                                    >
                                        Login
                                    </Button>
                                )
                            }
                        </Box>
                        { isLogged?(
                        <Box sx={{ flexGrow: 0 }}>
                            <Tooltip title="Open settings">
                                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    <Avatar alt="User" src="/static/images/avatar/2.jpg" />
                                </IconButton>
                            </Tooltip>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {settings.map((setting) => (
                                    <MenuItem key={setting} onClick={handleCloseUserMenu}>
                                        <Typography textAlign="center" onClick={settingsClicked}>{setting}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>):(<Box></Box>)
                        }
                    </Toolbar>
                </Container>
            </AppBar>
        </div>
    );
}

