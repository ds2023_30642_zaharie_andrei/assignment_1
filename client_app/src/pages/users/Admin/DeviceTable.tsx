import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import {Device} from "../User.types"
import Button from "@mui/material/Button";

import ModalEditDevice from "../Modals/ModalEditDevice";


interface DeviceTableProps {
    devices: Device[];
}

function Row(props: { device: Device, onEdit: (device: Device) => void}) {
    const { device, onEdit } = props;
    const [open, setOpen] = React.useState(false);

    const handleEditClick = () => {
        onEdit(device);
    };

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell>{device.name}</TableCell>
                <TableCell>{device.description}</TableCell>
                <TableCell>{device.address}</TableCell>
                <TableCell align="right">{device.energyConsumption} kWh</TableCell>
                <TableCell>
                    <Button
                        variant="outlined"
                        size="small"
                        onClick={handleEditClick}
                    >
                        Edit
                    </Button>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography variant="h6" gutterBottom component="div">
                                Description
                            </Typography>
                            <p>{device.description}</p>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}


const DeviceTable: React.FC<DeviceTableProps> = ({ devices }) => {
    const [open, setOpen] = React.useState(false);
    const [selectedDevice, setSelectedDevice] = React.useState<Device | null>(null);

    const handleEdit = (device: Device) => {
        setSelectedDevice(device);
        setOpen(true);
    };

    const handleModalClose = () => {
        setSelectedDevice(null);
        setOpen(false);
    };


    return (
        <TableContainer component={Paper}>
            <Table aria-label="device table">
                <TableHead>
                    <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                        <TableCell />
                        <TableCell>Name</TableCell>
                        <TableCell>Description</TableCell>
                        <TableCell>Address</TableCell>
                        <TableCell align="right">Energy Consumption (kWh)</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {devices.map((device, index) => (
                        <Row key={index} device={device} onEdit={handleEdit}/>
                    ))}
                </TableBody>
            </Table>
            <ModalEditDevice open={open} onClose={handleModalClose} device={selectedDevice}/>
        </TableContainer>
    );
}

export default DeviceTable;