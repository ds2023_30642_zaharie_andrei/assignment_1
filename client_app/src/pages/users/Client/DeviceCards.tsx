import React, {useContext} from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {Grid} from "@mui/material";
import { ThemeProvider } from '@emotion/react';
import CardsTheme from "./CardsTheme";
import ModalEditDevice from "../Modals/ModalEditDevice";
import ModalAcceptRemoveDevice from '../Modals/ModalAcceptRemoveDevice';
import {UserContext} from "../../UserProvider";
interface Device {
    name: string;
    description: string;
    address: string;
    energyConsumption: number;
    ownerGuid: string;
    id: number;
}

interface DeviceCardProps {
    device: Device;
    userGuid:string | null;
}

const DeviceCard: React.FC<DeviceCardProps> = ({ device, userGuid}) => {
    const [openEdit, setOpenEdit] = React.useState(false);
    const [openRemove, setOpenRemove] = React.useState(false);
    const [selectedDevice, setSelectedDevice] = React.useState<Device | null>(null);
    const {setRefreshPage} = useContext(UserContext);
    const handleEdit = (device: Device) => {
        setSelectedDevice(device);
        setOpenEdit(true);
    };

    const openRemoveModal = (device: Device) => {
        setSelectedDevice(device);
        setOpenRemove(true);
    };
    const handleRemove = async (deviceId: number) =>{
        if(deviceId){
            const result = await fetch(`http://localhost:8081/api/devices/removeOwner/${deviceId}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let data = await result.json();
            if (result.ok) {
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditDevice - error could not remove owner for device!");
            }
        }
        handleModalClose();
    }

    const handleModalClose = () => {
        setSelectedDevice(null);
        setOpenEdit(false);
        setOpenRemove(false);
    };


    return (
        <ThemeProvider theme={CardsTheme}>
        <Card sx={{ maxWidth: 345 }}>
            <CardMedia
                title={device.name}>
                <img
                    src="/devices.png"
                    alt="logo"
                    width="120"
                    height="120"
                />
            </CardMedia>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {device.name}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Description: {device.description}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Address: {device.address}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Energy Consumption: {device.energyConsumption} kWh
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" onClick={() => handleEdit(device)}>Edit</Button>
                <Button size="small" color={"error"} onClick={() => openRemoveModal(device)}>Remove</Button>
            </CardActions>
        </Card>
            { selectedDevice &&
                (
                    <div>
                    <ModalEditDevice open={openEdit} onClose={handleModalClose} device={selectedDevice} />
                    <ModalAcceptRemoveDevice open={openRemove} onClose={handleModalClose} onRemove={() =>handleRemove(selectedDevice?.id)} device={selectedDevice} />
                    </div>
                )
            }
        </ThemeProvider>

    );
}

export default DeviceCard;
