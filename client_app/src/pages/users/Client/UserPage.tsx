import React, {useState, useEffect, useContext} from 'react';
import DeviceCards from './DeviceCards'; // You can use the provided MediaCard component
import Button from '@mui/material/Button';
import {UserContext} from "../../UserProvider";
import {Device} from "../User.types";
import Grid from '@mui/material/Grid';

function UserPage() {

    const [devices, setDevices] = useState<Device[]>([]);
    const [userDevices, setUserDevices] = useState([]);
    const {refreshPage, setRefreshPage, userGuid} = useContext(UserContext);
    const [removeModal, setRemoveModal] = useState(false);

    useEffect(() => {
        async function fetchDevices() {
            try {
                const response = await fetch(`http://localhost:8081/api/devices/getUserDevices/${userGuid}`, {
                    method: 'GET'
                });

                if (response.ok) {
                    const data = await response.json();
                    setDevices(data);
                } else {
                    console.error('Failed to fetch devices:', response.status);
                }
            } catch (error) {
                console.error('Error fetching devices:', error);
            }
        }
            fetchDevices()
            .then(() => setRefreshPage(false));
    }, [refreshPage]);


    return (
        <div>
            <h1>User Page</h1>
            <div className={"user-cards"}>
            <Grid container spacing={2}>
            {devices.map((device:Device, index) => (
                <Grid item xs={4}>
                <DeviceCards
                    key={index}
                    device={device}
                    userGuid={userGuid}
                />
                </Grid>
            ))}
            </Grid>
            </div>
        </div>
    );
}

export default UserPage;
