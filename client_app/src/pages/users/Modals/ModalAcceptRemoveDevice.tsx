import React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import {Device} from "../User.types";
import '../User.styles.css'
interface RemoveDeviceModalProps {
    open: boolean;
    onClose: () => void;
    onRemove: () => void;
    device: Device;
}

const ModalAcceptRemoveDevice: React.FC<RemoveDeviceModalProps> = ({ open, onClose, onRemove, device }) => {
    return (
        <div className={"remove-modal"}>
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Remove Device</DialogTitle>
            <DialogContent>
                <h3>Are you sure you want to remove the device?</h3>
                <p><strong>{device.name}</strong></p>
                <p><strong>Energy Consumption</strong> : {device.energyConsumption} kWh</p>
                <p><strong>Address</strong> : {device.address}</p>
                <p><strong>Description</strong> : {device.description}</p>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={onRemove} color="error">
                    Remove
                </Button>
            </DialogActions>
        </Dialog>
        </div>
    );
};

export default ModalAcceptRemoveDevice;
