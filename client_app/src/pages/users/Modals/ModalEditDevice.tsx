import React, {useContext, useEffect, useState} from 'react';
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import {Device} from "../User.types";
import {TextField} from "@mui/material";
import '../User.styles.css'
import {UserContext} from "../../UserProvider";

interface ModalEditDeviceProps {
    open: boolean;
    onClose: () => void;
    device: Device | null;
}

const ModalEditDevice: React.FC<ModalEditDeviceProps> = ({open, onClose, device}) => {
    const [editedDevice, setEditedDevice] = useState<Device | null>();
    const {setRefreshPage} = useContext(UserContext);
    const [addingNewDevice, setAddingNewDevice] = useState(false);
    const role = localStorage.getItem("role");
    const [isAdmin, setIsAdmin] = useState(false);

    useEffect(() => {

        if(role === '[user]'){
            setIsAdmin(false);
        }else if(role === '[admin]'){
            setIsAdmin(true);
        }
        if (device) {
            console.log(device);
            setAddingNewDevice(false);
            setEditedDevice({...device});
        } else {
            console.log(addingNewDevice);
            setEditedDevice({
                name: "",
                description: "",
                address: "",
                energyConsumption: 0,
                ownerGuid: "",
                id: 0
            });
            setAddingNewDevice(true);
        }
    }, [device]);
    const handleUpdate = async () => {

        if (editedDevice) {
            const result = await fetch(`http://localhost:8081/api/devices/updateDevice/${editedDevice.id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(editedDevice)
            });
            let data = await result.json();
            if (result.ok) {
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditDevice - error could not update device!");
            }
        }
        onClose();
    };

    const handleDelete = async () => {
        if (editedDevice) {
            const result = await fetch(`http://localhost:8081/api/devices/deleteDevice/${editedDevice.id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let data = await result.json();
            if (result.ok) {
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditDevice - error could not delete device!");
            }
        }
        onClose();
    };

    const handleAdd = async () => {
        if (editedDevice) {
            const result = await fetch(`http://localhost:8081/api/devices/addDevice`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(editedDevice)
            });
            let data = await result.json();
            if (result.ok) {
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditDevice - error could not add new device!");
            }
        }
        onClose();
    };

    const handleRemoveOwner = async () => {
        if (editedDevice) {
            const result = await fetch(`http://localhost:8081/api/devices/removeOwner/${editedDevice.id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let data = await result.json();
            if (result.ok) {
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditDevice - error could not remove owner for device!");
            }
        }
        onClose();
    };

    return (
        <Modal open={open} onClose={onClose}>
            <div className={"modal-container"}>
                <form onSubmit={handleUpdate}>
                    {editedDevice && (
                        <div>
                            {addingNewDevice ? (<h2>Add new device</h2>) : (<h2>Edit device</h2>)}
                            <div className="form-container">
                                <TextField
                                    className="text-field"
                                    label="Name"
                                    value={editedDevice.name}
                                    onChange={(e) => setEditedDevice({...editedDevice, name: e.target.value})}
                                />
                                <TextField
                                    className="text-field"
                                    label="Description"
                                    value={editedDevice.description}
                                    onChange={(e) => setEditedDevice({...editedDevice, description: e.target.value})}
                                />
                                <TextField
                                    className="text-field"
                                    label="Address"
                                    value={editedDevice.address}
                                    onChange={(e) => setEditedDevice({...editedDevice, address: e.target.value})}
                                />
                                { isAdmin && (
                                <TextField
                                    className="text-field"
                                    label="Owner Guid"
                                    value={editedDevice.ownerGuid}
                                    onChange={(e) => setEditedDevice({...editedDevice, ownerGuid: e.target.value})}
                                />
                                )}
                                <TextField
                                    className="text-field"
                                    label="Energy Consumption"
                                    value={editedDevice.energyConsumption}
                                    onChange={(e) => setEditedDevice({
                                        ...editedDevice,
                                        energyConsumption: Number(e.target.value)
                                    })}
                                />
                                {addingNewDevice ? (
                                    <div className="add-modal-button">
                                        <Button variant="contained" color="primary" onClick={handleAdd}>
                                            Add
                                        </Button>
                                    </div>
                                ) : (
                                    isAdmin ? (
                                        <div className="buttons-container">
                                            <Button variant="contained" color="primary" onClick={handleUpdate}>
                                                Update
                                            </Button>
                                            <Button variant="outlined" color="error" onClick={handleDelete}>
                                                Delete
                                            </Button>
                                            <Button variant="outlined" color="error" onClick={handleRemoveOwner}>
                                                Remove Owner
                                            </Button>
                                        </div>
                                    ) : (
                                        <center>
                                            <Button variant="contained" color="primary" onClick={handleUpdate}>
                                                Update
                                            </Button>
                                        </center>
                                    )
                                )}
                            </div>
                        </div>
                    )}
                </form>
            </div>
        </Modal>
    );
}

export default ModalEditDevice;
