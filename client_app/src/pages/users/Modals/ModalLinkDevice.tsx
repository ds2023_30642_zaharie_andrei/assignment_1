import React, {useContext, useState} from 'react';
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Device, User } from "../User.types";
import {UserContext} from "../../UserProvider";
import "../User.styles.css"
interface LinkDevicesModalProps {
    open: boolean;
    onClose: () => void;
    user: User | null;
    devices: Device[];
}
type LinkDeviceRequest = {
    ownerGuid : string,
    deviceId: number,
}
const ModalLinkDevices: React.FC<LinkDevicesModalProps> = ({ open, onClose, user, devices }) => {
    const { setRefreshPage } = useContext(UserContext);
    const handleLinkDevice = async (deviceId: number) => {
        try {
            if(user) {
                const linkDeviceRequest : LinkDeviceRequest = {
                    ownerGuid: user.guid,
                    deviceId: deviceId,
                }

                const response = await fetch(`http://localhost:8081/api/devices/deviceLink`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body:JSON.stringify(linkDeviceRequest)
                });
                const data = await  response.json();
                if (response.ok) {
                    console.log(data);
                    console.log('Device linked successfully.');
                    setRefreshPage(true);

                } else {
                    console.error('Failed to link device:', response.status);
                }
            }
        } catch (error) {
            console.error('Error linking device:', error);
        }
    };

    return (
        <Modal open={open} onClose={onClose}>
            <div className="modal-container">
                {user&&
                    (<h2>User: {user.name}</h2>)
                }
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Available Device</TableCell>
                            <TableCell>Device Name</TableCell>
                            <TableCell>Description</TableCell>
                            <TableCell>Address</TableCell>
                            <TableCell>Energy Cons.</TableCell>
                            <TableCell>Link</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {devices.filter(d => d.ownerGuid === null || d.ownerGuid === "").map((device, index) => (
                            <TableRow key={device.id}>
                                <TableCell>{index + 1}</TableCell>
                                <TableCell>{device.name}</TableCell>
                                <TableCell>{device.description}</TableCell>
                                <TableCell>{device.address}</TableCell>
                                <TableCell>{device.energyConsumption}</TableCell>
                                <TableCell>
                                    <Button variant="contained" color="primary" onClick={() => handleLinkDevice(device.id)}>
                                        Link
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                    <div className="buttons-container">
                    <Button variant="outlined" color="error" onClick={onClose}>
                        Close
                    </Button>
                    </div>
                </Table>
            </div>
        </Modal>
    );
};

export default ModalLinkDevices;
