export type Device ={
    name: string;
    description: string;
    address: string;
    energyConsumption: number;
    ownerGuid : string;
    id : number;
}

export type User = {
    guid: string
    email: string;
    name: string;
    role: string;
    deviceList: Device[];
}

export type UserRequest = {
    guid: string
    email: string;
    name: string;
    role: string;
}