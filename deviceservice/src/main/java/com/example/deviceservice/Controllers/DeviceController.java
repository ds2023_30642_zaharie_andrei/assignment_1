package com.example.deviceservice.Controllers;

import com.example.deviceservice.Controllers.Models.LinkDeviceRequest;
import com.example.deviceservice.Repository.Models.Device;
import com.example.deviceservice.Services.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/devices")
@CrossOrigin()
public class DeviceController {
    private final DeviceService deviceService;


    @GetMapping("/getDevice")
    public ResponseEntity<?> getDevice(@RequestAttribute int id){
        Device device = this.deviceService.getDevice(id);
        return ResponseEntity.ok(device);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllDevices(){
        List<Device> devices = this.deviceService.getAllDevice();

        return ResponseEntity.ok(devices);
    }

    @PostMapping("/addDevice")
    public ResponseEntity<?> addDevice(@RequestBody Device newDevice){
        Device device = deviceService.save(newDevice);

        return ResponseEntity.ok(device);
    }

    @DeleteMapping("/deleteDevice/{deviceId}")
    public ResponseEntity<?> deleteDevice(@PathVariable long deviceId){
        deviceService.deleteDevice(deviceId);

        return ResponseEntity.ok(HttpEntity.EMPTY);
    }

    @DeleteMapping("/deleteUserDevices/{userGuid}")
    public ResponseEntity<?> deleteUserDevices(@PathVariable String userGuid){
        deviceService.deleteUserDevices(userGuid);

        return ResponseEntity.ok(HttpEntity.EMPTY);
    }

    @PostMapping("/updateDevice/{deviceId}")
    public ResponseEntity<Device> updateDevice(@RequestBody Device updateDevice, @PathVariable long deviceId)
    {
        Device device = deviceService.update(deviceId, updateDevice);

        return ResponseEntity.ok(device);
    }

    @PostMapping("/removeOwner/{deviceId}")
    public ResponseEntity<?> removeOwner(@PathVariable long deviceId)
    {
        deviceService.removeOwner(deviceId);

        return ResponseEntity.ok(HttpEntity.EMPTY);
    }

    @PostMapping("/deviceLink")
    public ResponseEntity<?> deviceLink(@RequestBody LinkDeviceRequest request)
    {
        Device linkedDevice = deviceService.linkDevice(request);

        return ResponseEntity.ok(linkedDevice);
    }

    @GetMapping("/getUserDevices/{userGuid}")
    public ResponseEntity<?> getUserDevices(@PathVariable String userGuid)
    {
        List<Device> devices = deviceService.getAllUserDevices(userGuid);

        return ResponseEntity.ok(devices);
    }
}
