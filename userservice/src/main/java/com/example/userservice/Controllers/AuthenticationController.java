package com.example.userservice.Controllers;

import com.example.userservice.Controllers.Models.AuthenticationRequest;
import com.example.userservice.Controllers.Models.AuthenticationResponse;
import com.example.userservice.Controllers.Models.RegisterRequest;
import com.example.userservice.Services.AuthenticationService;
import com.example.userservice.Services.Security.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final JwtService jwtService;
    private final AuthenticationService authService;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(@RequestBody RegisterRequest request){
        return ResponseEntity.ok(authService.registerUser(request));
    }

    @PostMapping(value = "/login")
    public ResponseEntity<AuthenticationResponse> createAuthenticationToken(
            @RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        return ResponseEntity.ok(authService.loginUser(authenticationRequest));
    }
}
