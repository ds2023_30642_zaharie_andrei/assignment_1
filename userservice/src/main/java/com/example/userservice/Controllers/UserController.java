package com.example.userservice.Controllers;

import com.example.userservice.Controllers.Models.UserRequest;
import com.example.userservice.Controllers.Models.UserResponse;
import com.example.userservice.Services.AuthenticationService;
import com.example.userservice.Services.UserService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000/", allowedHeaders = "*")
@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final AuthenticationService authenticationService;

    @GetMapping("{guid}")
    public ResponseEntity<UserResponse> getUser(@PathVariable String guid){
        UserResponse user = this.userService.find(guid);

        return ResponseEntity.ok(user);
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserResponse>> getAllUsers(){
        List<UserResponse> users = this.userService.findAll();

        return ResponseEntity.ok(users);
    }

    @PostMapping("/addUser")
    public ResponseEntity<UserResponse> addUser(@RequestBody UserRequest user){
        String encodedPassword = authenticationService.encodePassword(user.getPassword());
        user.setPassword(encodedPassword);
        UserResponse savedUser = this.userService.save(user);

        return ResponseEntity.ok(savedUser);
    }

    @PostMapping("/updateUser/{guid}")
    public ResponseEntity<UserResponse> updateUser(@PathVariable String guid, @RequestBody UserRequest user){
        UserResponse updateUser = this.userService.update(guid,user);

        return ResponseEntity.ok(updateUser);
    }

    @DeleteMapping("/deleteUser/{guid}")
    public ResponseEntity<?> deleteUser(@PathVariable String guid){
        this.userService.delete(guid);

        return ResponseEntity.ok(HttpEntity.EMPTY);
    }

    @GetMapping("/hello")
    public ResponseEntity<?> hello(){
        return new ResponseEntity<>("Hello from my first jwt token", HttpStatus.OK);
    }
}
