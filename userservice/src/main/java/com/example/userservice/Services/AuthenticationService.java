package com.example.userservice.Services;

import com.example.userservice.Controllers.Models.AuthenticationRequest;
import com.example.userservice.Controllers.Models.AuthenticationResponse;
import com.example.userservice.Controllers.Models.RegisterRequest;
import com.example.userservice.Repository.IUserRepository;
import com.example.userservice.Repository.Models.User;
import com.example.userservice.Services.Converters.UserRegisterConvertors;
import com.example.userservice.Services.Security.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final IUserRepository repository;
    private final UserRegisterConvertors userRegisterConvertors;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;

    public AuthenticationResponse registerUser(RegisterRequest request){
        User newUser = userRegisterConvertors.toModel(request);
        newUser.setPassword(passwordEncoder.encode(request.getPassword()));
        repository.save(newUser);

        String token = jwtService.generateToken(newUser);

        return new AuthenticationResponse(token);
    }

    public AuthenticationResponse loginUser(AuthenticationRequest request){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        User user = repository.findUserByEmail(request.getEmail()).orElseThrow(
                ()-> new RuntimeException("Cannot find the user with the email: " + request.getEmail()));


        String token = jwtService.generateToken(user);

        return new AuthenticationResponse(token);
    }

    public String encodePassword(String password){
        return this.passwordEncoder.encode(password);
    }
}
