package com.example.userservice.Services;

import com.example.userservice.Controllers.Models.UserRequest;
import com.example.userservice.Controllers.Models.UserResponse;
import com.example.userservice.Repository.Models.User;

import java.util.List;

public interface IUserService {
    UserResponse save(UserRequest user);
    UserResponse find(String guid);
    void delete(String guid);
    UserResponse update(String guid, UserRequest updatedUser);
    User findByEmail(String email);

    List<UserResponse> findAll();
}
